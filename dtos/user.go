package dtos

type UserServiceResponseStruct struct {
	Uuid  string `json:"uuid"`
	PType string `json:"ptype"`
}

type UserServiceRequestStruct struct {
	Uuid  string `json:"uuid"`
	PType string `json:"ptype"`
}
