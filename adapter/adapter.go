package adapter

import (
	"encoding/json"
	"io/ioutil"
	"kong-go-plugin/dtos"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

type adapterStruct struct {
	BaseURL string
	log     *logrus.Logger
}

type AdapterI interface {
	GetUuidandProfileType(userAuth string) (*dtos.UserServiceResponseStruct, error)
}

func New(baseURL string, l *logrus.Logger) AdapterI {

	return &adapterStruct{
		BaseURL: baseURL,
		log:     l,
	}
}

func (w *adapterStruct) GetUuidandProfileType(userAuth string) (*dtos.UserServiceResponseStruct, error) {

	url := w.BaseURL + ""

	w.log.Debugf("end point for updating headers in request transformer : %s", url)

	/*var userServiceRequest *dtos.UserServiceRequestStruct
	reqBody, err := json.Marshal(userServiceRequest)
	if err != nil {
		w.log.Errorf("unable to marshal request body : %v", err)
		return nil, err
	}
	w.log.Debugf("client has the following request body : %v", string(reqBody))
	*/
	request, err := http.NewRequest(
		http.MethodGet,
		url,
		//bytes.NewBuffer(reqBody)
		nil,
	)
	if err != nil {
		w.log.Errorf("unable to create new %v request: %v", "GET", err)
		return nil, err
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("x-gommt-user-auth", userAuth)

	w.log.Debugf("client has the following request Headers : %v", request.Header)

	tr := &http.Transport{}

	client := http.Client{
		Timeout:   time.Duration(60) * time.Second,
		Transport: tr,
	}
	resp, err := client.Do(request)
	if err != nil {
		w.log.Errorf("unable to send Request: %v", err)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		w.log.Errorf("unable to read response body : %v", err)
		return nil, err
	}

	w.log.Debugf("http status  endpoint: %d", resp.StatusCode)
	w.log.Debugf("response from the  endpoint: %s", string(body))

	var res dtos.UserServiceResponseStruct
	if err := json.Unmarshal(body, &res); err != nil {
		w.log.Errorf("error unmarshalling the response: %v", err)
		return nil, err
	}
	return &res, nil
}
