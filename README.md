# kong-go-plugin-sample

It adds an extra layer for security between consumers and producers. Consumers send a consumer-key in a query string and in this way we are able to identify the consumers. Without this parameter or wrong parameter won't be worked and they will get an error message regarding that.

We define the key for a specific consumer and it has to send this key as a query string. After that we are matching the key in query-string and pre-defined key in configuration file.

```
docker build -t kongdemo .
```

Images are built and we can run them as containers.

```
docker run -d --name kong \
    --network=kong-ee-net \
    -e "KONG_DATABASE=postgres" \
    -e "KONG_PG_HOST=kong-database" \
    -e "KONG_PG_PASSWORD=kong" \
    -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
    -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
    -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
    -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
    -e "KONG_ADMIN_LISTEN=0.0.0.0:8001 ssl" \
    -e "KONG_PROXY_LISTEN=0.0.0.0:8000 ssl" \
    -e "KONG_GO_PLUGINS_DIR=/tmp/go-plugins" \
    -e "KONG_PLUGINS=bundled,key-checker" \
    -e "SSL_CERT=/tmp/key-checker/certificate/cert.pem" \
    -e "SSL_CERT_KEY=/tmp/key-checker/certificate/key.pem" \
    -e "ADMIN_SSL_CERT=/tmp/key-checker/certificate/cert.pem" \
    -e "ADMIN_SSL_CERT_KEY=/tmp/key-checker/certificate/key.pem" \
 -p 8000:8000 \
-p 8443:8443 \
-p 8001:8001 \
-p 8444:8444 \
-p 8002:8002 \
-p 8445:8445 \
-p 8003:8003 \
-p 8004:8004 \
    kongdemo
```
