package main

import (
	"encoding/json"
	"fmt"

	"kong-go-plugin/adapter"

	pdk "github.com/Kong/go-pdk"
	"github.com/sirupsen/logrus"
)

type Config struct {
	Apikey string
}

func New() interface{} {
	return &Config{}
}

func (conf Config) Access(kong *pdk.PDK) {
	kong.ServiceRequest.SetHeader("x-hello-from-plugin", fmt.Sprintf("Go says %s to %s", "message", "host"))
	kong.Response.SetHeader("x-hello-from-go", fmt.Sprintf("Go says %s to %s", "message", "host"))
	rawQuery, _ := kong.Request.GetRawQuery()
	kong.Response.SetHeader("x-query-param-string", rawQuery)
	headers, _ := kong.Request.GetHeaders(100)
	byteHeaders, _ := json.Marshal(headers)
	kong.Response.SetHeader("x-all-headers", string(byteHeaders))
	//x-gommt-uuid: U1-AUTH
	//x-gommt-profileType: U1-AUTH

	auth, _ := kong.Request.GetHeader("Authorization")
	if auth != "" {
		userAdapter := adapter.New(
			"https://f50ee46c8c6b211509d308e147962788.m.pipedream.net",
			logrus.New(),
		)
		res, err := userAdapter.GetUuidandProfileType(auth)
		if err == nil {
			kong.ServiceRequest.SetHeader("x-gommt-uuid", res.Uuid)
			kong.ServiceRequest.SetHeader("x-gommt-profileType", res.PType)
		} else {
			kong.ServiceRequest.SetHeader("x-gommt-error", err.Error())
		}
	}
}
